package movie_lens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac007 on 2017/5/15.
  */
object TopRatingsApp extends App {
    val conf = new SparkConf().setAppName("TopRatingsApp").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val ratings: RDD[(Int, Double)] = sc.textFile("/Users/mac007/Downloads/ml-20m/ratings.csv")
      .map(str => str.split(","))        //用逗號去分割
      .filter(strs => strs(1) != "movieId") //去掉第一行：userId,movieId,rating,timestamp
      .map(strs => {
        //println(strs.mkString(","))
        (strs(1).toInt, strs(2).toDouble)
    })
    //列５個出來
    ratings.take(5).foreach(println)

    //false:不取後放回；5：隨機抽取五個
    ratings.takeSample(false,5).foreach(println)

    //隨機抽取五個電影，並計算其總評分
    val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
     totalRatingByMovieId.takeSample(false,5).foreach(println)

    //隨機５個電影的平均評分
    val averageRatingsByMovieId:RDD[(Int,Double)]=ratings.mapValues(v=>v->1)
      .reduceByKey((acc,curr)=>{(acc._1+curr._1)->(acc._2+curr._2)})
      .mapValues(kv=> kv._1 / kv._2.toDouble)
    averageRatingsByMovieId.takeSample(false,5).foreach(println)

    //找出平均評分最高的前１０部電影
    val top10=averageRatingsByMovieId.sortBy(_._2,false).take(10)
    top10.foreach(println)

    //讀取movies.csv
    val movies=sc.textFile("/Users/mac007/Downloads/ml-20m/movies.csv")
      .map(str => str.split(","))
      .filter(strs => strs(0) != "movieId")  //第０個不要出現movieId(第一個：title;第二個：genres)
      .map(strs=>strs(0).toInt->strs(1))
    movies.take(5).foreach(println)

    //結合average跟movies的結果（顯示出電影名稱）
    val joined: RDD[(Int,(Double,String))]=averageRatingsByMovieId.join(movies)
    joined.takeSample(false,5).foreach(println)

    //找出前５名的電影名稱
    val top5=joined.sortBy(_._2,false).take(5)

    //將結果存在reult檔
    joined.saveAsTextFile("result")

    //存成csv檔
    joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result")




}


