import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac007 on 2017/5/8.
  */
object WordCountApp extends App{
  val conf=new SparkConf().setAppName("KeyValues").setMaster("local[*]")
  val sc= new SparkContext(conf)

  val lines=sc.textFile("/Users/mac007/Downloads")


  val words=lines.flatMap(i=>i.split(" "))

  //
  words.groupBy(str=>str).mapValues(_.size).take(10).foreach(println)

  //
 words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr)
    .take(10)
    .foreach(println)

  readLine()

}
