import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac007 on 2017/5/8.
  */
object FilterDemoApp extends App{
  val conf=new SparkConf().setAppName("FilterDemo").setMaster("local[*]")
  val sc=new SparkContext(conf)

  //odds
 sc.parallelize(1 to 100).filter(i=>i%2==1)
   .foreach(v=>println(v))

  //first Letter Is M
  sc.textFile("./names.txt")
    .filter(name=>name.startsWith("M"))
    .foreach(println)
}
