import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac007 on 2017/5/8.
  */
object keyValueApp extends App{
  val conf=new SparkConf().setAppName("KeyValues").setMaster("local[*]")
  val sc= new SparkContext(conf)

//1 to 100 odd or even
  val kvRdd=sc.parallelize(1 to 100).map(v=>{
    if(v%2==0)"even"->v else "odd"->v
  })


//take 10:odd or even
  kvRdd.mapValues(_+1).take(10).foreach(println)

  println("======group by key=======")

  //group:odd/even  ; sum:odd/even
  kvRdd.groupByKey()
    .mapValues(v=>v.sum)
    .foreach(println)

  println("======reduce by key========")

  //reduce
  kvRdd.reduceByKey((acc,curr)=>acc+curr).foreach(println)

}
